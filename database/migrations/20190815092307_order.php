<?php

use think\migration\Migrator;
use think\migration\db\Column;

class Order extends Migrator
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $table  =  $this->table('order',['engine'=>'InnoDB','comment'=>'订单表']);
        $table->addColumn('user_id', 'integer',['limit'=>11,'default'=>0,'comment'=>'购买用户id'])
            ->addColumn('merchant_id', 'integer',['limit'=>11,'default'=>0,'comment'=>'商户id'])
            ->addColumn('trade_no', 'string',['limit'=>32,'default'=>'','comment'=>'系统交易订单号'])
            ->addColumn('out_trade_no', 'string',['limit'=>32,'default'=>'','comment'=>'第三方支付订单号'])
            ->addColumn('order_amount', 'decimal',['precision'=>10,'scale'=>2,'default'=>0,'comment'=>'订单金额'])
            ->addColumn('discount_amount', 'decimal',['precision'=>10,'scale'=>2,'default'=>0,'comment'=>'折扣金额'])
            ->addColumn('final_amount', 'decimal',['precision'=>10,'scale'=>2,'default'=>0,'comment'=>'最终付款金额'])
            ->addColumn('pay_time', 'datetime',['default'=>null,'null'=>true,'comment'=>'支付时间'])
            ->addColumn('status', 'boolean',['limit'=>1,'default'=>1,'comment'=>'订单状态，状态信息见配置'])
            ->addColumn('created_at', 'datetime',['default'=>'2000-01-01 00:00:00','comment'=>'创建时间'])
            ->addColumn('updated_at', 'datetime',['default'=>null,'comment'=>'更新时间','null'=>true])
            ->addColumn('deleted_at', 'datetime',['default'=>null,'comment'=>'软删除时间','null'=>true])
            ->addIndex(['user_id'], ['name'=>'idx_user_id'])
            ->addIndex(['merchant_id'], ['name'=>'idx_merchant_id'])
            ->addIndex(['trade_no'], ['name'=>'idx_trade_no'])
            ->create();
    }
}
