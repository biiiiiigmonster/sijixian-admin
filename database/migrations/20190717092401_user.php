<?php

use think\migration\Migrator;
use think\migration\db\Column;

class User extends Migrator
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        // create the table
        $table  =  $this->table('user',['engine'=>'InnoDB','comment'=>'用户表']);
        $table->addColumn('mobile', 'string',['limit'=>18,'default'=>'','comment'=>'手机号，登陆使用'])
            ->addColumn('password', 'string',['limit'=>32,'default'=>md5('abc12345'),'comment'=>'用户密码'])
            ->addColumn('login_status', 'boolean',['limit'=>1,'default'=>0,'comment'=>'登陆状态'])
            ->addColumn('login_code', 'string',['limit'=>32,'default'=>'','comment'=>'排他性登陆标识'])
//            ->addColumn('last_login_ip', 'string',['limit'=>16,'default'=>'','comment'=>'最后登录IP'])
//            ->addColumn('last_login_time', 'datetime',['default'=>null,'comment'=>'最后登录时间','null'=>true])
            ->addColumn('status', 'boolean',['limit'=>1,'default'=>0,'comment'=>'账号状态，状态信息见配置'])
            ->addColumn('created_at', 'datetime',['default'=>'2000-01-01 00:00:00','comment'=>'创建时间'])
            ->addColumn('updated_at', 'datetime',['default'=>null,'comment'=>'更新时间','null'=>true])
            ->addColumn('deleted_at', 'datetime',['default'=>null,'comment'=>'软删除时间','null'=>true])
            ->addIndex(['mobile'], ['unique'=>true])
            ->create();
    }
}
