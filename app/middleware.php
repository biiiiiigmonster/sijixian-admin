<?php

return [
    // 全局请求缓存
//     \think\middleware\CheckRequestCache::class,
    // 多语言加载
     \think\middleware\LoadLangPack::class,
    // Session初始化
    // \think\middleware\SessionInit::class,
    // 页面Trace调试
    // \think\middleware\TraceDebug::class,

    //接口形式相关中间件
//    //token授权中间件
//    'Authorize'    => app\http\middleware\api\Authorize::class,//负责生成用户授权标识
//    //token认证中间件
//    'Authenticate'    => app\http\middleware\api\Authenticate::class,//负责验证用户授权信息
//    //验证签名中间件
//    'Signature'    => app\http\middleware\api\Signature::class,//这个验证比较严格，一般小项目不要轻易使用
//    //请求频率限制中间件
//    'Throttle'    => app\http\middleware\api\Throttle::class,//这个验证比较严格，一般小项目不要轻易使用
//    //返回结果包装中间件
//    'Launcher'    => app\http\middleware\api\Launcher::class,//响应结果发射器，主要用于统一格式
//
//    //web嵌套模式相关中间件
//    //嵌套模式身份生成中间件
//    'Session'    => app\http\middleware\web\Session::class,//负责生成用户授权缓存
];
