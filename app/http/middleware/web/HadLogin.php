<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 2018/10/15
 * Time: 11:22
 */

namespace app\http\middleware\web;

use think\facade\Session;
use Closure;
/**
 * 如果当前存在登录信息，自动跳转
 * 使用前提为服务器端记录登录凭证的时候，例如Session
 * 该中间件适用场景：如登录页、注册页、找回密码页等...
 * 功能比较单一，适用范围也比较窄
 * Class HadLogin
 * @package app\http\middleware
 */
class HadLogin
{
    /**
     * “已登录”验证
     * @param $request
     * @param \Closure $next
     * @return mixed|\think\response\Redirect
     */
    public function handle($request, Closure $next)
    {
        //当前模块下默认跳转路径
        $index = config('app.default_controller').'/'.config('app.default_action');
        //当前作用域下如果存在登录session信息，则自动跳转
        if(Session::has('loginInfo'))
            return redirect($index);

        return $next($request);
    }
}