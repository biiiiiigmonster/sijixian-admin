<?php


namespace app\http\middleware;


use app\common\traits\Client;
use Closure;
use think\facade\Cache;
use think\facade\Log;
use think\Response;

//这个类好像没什么用，到时候删掉吧
class DoubleClickProtection
{
    use Client;

    public function handle($request, Closure $next):Response
    {
        $terminalCode = $this->getTerminalCode();//获取当前设备唯一标识，该标识也作为系统中计数统计的唯一标识
        if(!$terminalCode) {
            return json('Precondition Failed')->code(412);
        }
        
        if(Cache::get($terminalCode)) {
            return json('Too Many Requests')->code(503);
        }
        Cache::set($terminalCode,1,2);

        $response = $next($request);

        Log::record('jinlailo');
        Cache::pull($terminalCode);

        return $response;
    }
}