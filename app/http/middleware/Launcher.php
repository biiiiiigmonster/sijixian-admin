<?php

namespace app\http\middleware;

use app\common\exception\BizException;
use Closure;
use think\Exception;
use think\db\exception\ModelNotFoundException;
use think\exception\ErrorException;
use think\exception\RouteNotFoundException;
use think\exception\ValidateException;
use think\facade\Log;
use think\helper\Arr;
use think\Response;
use think\response\Json;

/**
 * 发射器
 * 将控制器方法的执行结果去安全的响应请求
 * 实际上发射器主要的目的是要将结果分发出去，异常部分处理没有问题，但是预期部分却因为不同的场景会有不同需求
 * 总之发射器需要满足各类需求场景，稳妥的将结果响应给请求者
 * Class Launcher
 * @package app\http\middleware
 */
class Launcher
{
    /**
     * api返回数据格式化中间件
     * @param $request
     * @param Closure $next
     * @return Response
     */
    public function handle($request, Closure $next):Response
    {
        $response = $next($request);
        $data = $response->getData();

        if(!isset($data['status'])) {
            $response->data(format($data));
        }
        return $response;
    }
}