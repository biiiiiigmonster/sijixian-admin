<?php

return [
    //控制器：首字母大写， 其余全小写
    'User' => [
        //action：全小写
        'login' => ['mobile', 'password'],
        'register' => ['register', 'password'],
        'myinfo' => ['mobile', 'password'],
        'update' => ['mobile', 'password'],
        'findpwd' => ['mobile', 'password'],
    ],
];