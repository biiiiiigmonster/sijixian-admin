<?php
use think\facade\Route;
use app\api\validate\Index;

Route::miss('Error/miss');
Route::get('/config',function (){
    return json(config('label'));
});
Route::get('/','Index/index')->validate(Index::class,'index');
