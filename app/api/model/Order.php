<?php

namespace app\api\model;

use think\Model;

/**
 * @mixin think\Model
 */
class Order extends Model
{
    protected $connection = 'mysql1';
    //
    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
