<?php

namespace app\api\model;

use think\Model;

/**
 * @mixin think\Model
 */
class User extends Model
{
    protected $hidden = ['password','delete_time','login_code'];
}
