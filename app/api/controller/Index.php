<?php

namespace app\api\controller;

use app\api\biz\IndexBiz;
use app\api\model\Order;
use app\BaseController;
use app\common\exception\ApiException;
use app\common\exception\BizException;

class Index extends BaseController
{
    /**
     * 菜品列表
     * @return \think\response\Json
     * @throws \Exception
     */
    public function index()
    {
        $data = $this->request->param();

        $result = (IndexBiz::instance())->index($data);

        return json($result);
    }
}