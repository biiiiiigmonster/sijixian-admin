<?php

namespace app\api\biz;

use app\api\model\Order;
use app\common\biz\BaseBiz;

class IndexBiz extends BaseBiz
{
    protected function index(array $param)
    {
        return [
            ['name' => '白萝卜', 'desc' => '', 'price' => '2.96/斤', 'label_id' => 1, 'thumbUrl' => 'http://admin.sijixian.duzhaoteng.com/static/images/bailuobo.jpg'],
            ['name' => '鳊鱼', 'desc' => '', 'price' => '14.96/斤', 'label_id' => 3, 'thumbUrl' => 'http://admin.sijixian.duzhaoteng.com/static/images/jiyu.jpg'],
            ['name' => '冬瓜', 'desc' => '', 'price' => '2.49/斤', 'label_id' => 1, 'thumbUrl' => 'http://admin.sijixian.duzhaoteng.com/static/images/donggua.jpg'],
            ['name' => '杭椒', 'desc' => '', 'price' => '5.96/斤', 'label_id' => 1, 'thumbUrl' => 'http://admin.sijixian.duzhaoteng.com/static/images/hangjiao.jpg'],
            ['name' => '红菜苔', 'desc' => '', 'price' => '3.49/斤', 'label_id' => 1, 'thumbUrl' => 'http://admin.sijixian.duzhaoteng.com/static/images/hongcaitai.jpg'],
            ['name' => '红薯', 'desc' => '', 'price' => '4.96/斤', 'label_id' => 1, 'thumbUrl' => 'http://admin.sijixian.duzhaoteng.com/static/images/hongshu.jpeg'],
            ['name' => '黄骨鱼', 'desc' => '', 'price' => '16.96/斤', 'label_id' => 3, 'thumbUrl' => 'http://admin.sijixian.duzhaoteng.com/static/images/huangguyu.jpg'],
            ['name' => '鲫鱼', 'desc' => '', 'price' => '11.96/斤', 'label_id' => 3, 'thumbUrl' => 'http://admin.sijixian.duzhaoteng.com/static/images/jiyu.jpg'],
            ['name' => '青椒', 'desc' => '', 'price' => '4.40/斤', 'label_id' => 1, 'thumbUrl' => 'http://admin.sijixian.duzhaoteng.com/static/images/qingjiao.jpg'],
            ['name' => '生菜', 'desc' => '', 'price' => '3.49/斤', 'label_id' => 1, 'thumbUrl' => 'http://admin.sijixian.duzhaoteng.com/static/images/shengcai.jpg'],
            ['name' => '娃娃菜', 'desc' => '', 'price' => '3.99/斤', 'label_id' => 1, 'thumbUrl' => 'http://admin.sijixian.duzhaoteng.com/static/images/wawacai.jpg'],
            ['name' => '五花肉', 'desc' => '', 'price' => '27.98/斤', 'label_id' => 2, 'thumbUrl' => 'http://admin.sijixian.duzhaoteng.com/static/images/wuhuarou.jpg'],
            ['name' => '鲜鸡蛋', 'desc' => '', 'price' => '4.96/斤', 'label_id' => 2, 'thumbUrl' => 'http://admin.sijixian.duzhaoteng.com/static/images/xianjidan.jpg'],
            ['name' => '油麦菜', 'desc' => '', 'price' => '2.98/斤', 'label_id' => 1, 'thumbUrl' => 'http://admin.sijixian.duzhaoteng.com/static/images/youmaicai.jpg'],
        ];
    }
}
