<?php


namespace app\api\middleware;


use app\api\model\User;
use Closure;
use think\Response;

class Account
{
    /**
     * 在请求中注入用户信息
     * @param $request
     * @param Closure $next
     * @return mixed
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public function handle($request, Closure $next):Response
    {
        // 从授权对象中获取该模块的用户主键，一般默认为id
        // 此处假设用户模型为user
        // 并且此处也没有强制一定解析出用户信息，即直接解析结果注入到请求与逻辑层中
        $request->auth && $request->account = User::find($request->auth->id);//如果原始请求中有传递account参数，那么会被覆盖，所以在定义这个的时候就记得要避免冲突了

        return $next($request);
    }
}