<?php


namespace app\common\exception;


class BizException extends \Exception
{
    private $bizCode;
    /**
     * BizException constructor.
     * @param int $code
     * @param int $bizCode
     * @param string $message
     */
    public function __construct($message, $bizCode, $code = 200)
    {
        $this->message = $message;
        $this->bizCode = $bizCode;
        $this->code    = $code;
    }

    public function getBizCode()
    {
        return $this->bizCode;
    }
}