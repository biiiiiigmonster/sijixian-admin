<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 2019/3/5
 * Time: 14:49
 */

namespace app\common\command;


use think\console\command\Make;
use think\facade\App;
use think\facade\Config;

class Controller extends Make
{
    protected $type = "Controller";

    protected function configure()
    {
        parent::configure();
        $this->setName('make:newController')
            ->setDescription('Create a new resource controller class');
    }

    protected function getStub()
    {
        $stubPath = __DIR__ . DIRECTORY_SEPARATOR . 'stubs' . DIRECTORY_SEPARATOR;

        return $stubPath . 'controller.stub';
    }

    protected function getClassName(string $name):string
    {
        return parent::getClassName($name);
    }

    protected function getNamespace(string $appNamespace):string
    {
        return parent::getNamespace($appNamespace) . '\controller';
    }

    protected function buildClass(string $name)
    {
        $stub = file_get_contents($this->getStub());

        $namespace = trim(implode('\\', array_slice(explode('\\', $name), 0, -1)), '\\');

        $class = str_replace($namespace . '\\', '', $name);

        $bizNamespace = trim(implode('\\', array_slice(explode('\\', $name), 0, -2)), '\\').'\\biz\\'.$class.'Biz';

        return str_replace(['{%className%}', '{%actionSuffix%}', '{%namespace%}', '{%bizNamespace%}', '{%app_namespace%}'], [
            $class,
            Config::get('action_suffix'),
            $namespace,
            $bizNamespace,
            App::getNamespace(),
        ], $stub);
    }
}