<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 2019/3/5
 * Time: 14:27
 */

namespace app\common\command;

use think\console\command\Make;

class Biz extends Make
{
    protected $type = "Biz";

    protected function configure()
    {
        parent::configure();
        $this->setName('make:biz')
            ->setDescription('Create a new resource biz class');
    }

    protected function getStub()
    {
        $stubPath = __DIR__ . DIRECTORY_SEPARATOR . 'stubs' . DIRECTORY_SEPARATOR;

        return $stubPath . 'biz.stub';
    }

    protected function getClassName(string $name):string
    {
        return parent::getClassName($name);
    }

    protected function getNamespace(string $appNamespace):string
    {
        return parent::getNamespace($appNamespace) . '\biz';
    }
}