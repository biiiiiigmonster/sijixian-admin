<?php

namespace app\subscribe;

use app\auth\model\User as UserModel;

class User
{
    public function onUserLogin($user)
    {
        $user['last_login_ip'] = request()->ip();
        $user['last_login_time'] = date('Y-m-d H:i:s');
        UserModel::update($user);
    }

    public function onUserRegister($user)
    {
        $user['last_login_ip'] = request()->ip();
        $user['last_login_time'] = date('Y-m-d H:i:s');
        UserModel::update($user);
    }

    public function onUserLogout($user)
    {
        $user['login_status'] = 0;
        UserModel::update($user);
    }
}
