<?php
declare (strict_types = 1);

namespace app\subscribe;

class Points
{
    public function onUserLogin($user)
    {
        echo '[PointsSubscribe]';
        event('PointsChange',$user);
//        var_dump($user);
        // UserLogin事件响应处理
    }

    public function onPointsChange($user)
    {
        echo '积分变更通知';
    }
}
